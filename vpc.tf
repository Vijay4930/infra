#####################################################VPC#####################################################
resource "aws_vpc" "my-first-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    "Name"       = "my-first-vpc"
    "created By" = "vijay"
  }
}
#######################################################Public Subnets##########################################
resource "aws_subnet" "my-public-subnet-1" {
  vpc_id                  = aws_vpc.my-first-vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "my-public-subnet-1"
  }
}

resource "aws_subnet" "my-public-subnet-2" {
  vpc_id                  = aws_vpc.my-first-vpc.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "my-public-subnet-2"
  }
}

resource "aws_subnet" "my-public-subnet-3" {
  vpc_id                  = aws_vpc.my-first-vpc.id
  cidr_block              = "10.0.3.0/24"
  availability_zone       = "us-east-1c"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "my-public-subnet-3"
  }
}
#########################################################IGW####################################

resource "aws_internet_gateway" "my-internet-gateway" {
  vpc_id = aws_vpc.my-first-vpc.id
  tags = {
    "Name" = "My-Internet-Gateway"
  }

}

########################################################Route Table############################

resource "aws_route_table" "My-Route-Table" {
  vpc_id = aws_vpc.my-first-vpc.id
  tags = {
    "Name" = "My-Route-Table"
  }
}

resource "aws_route" "My-public-route" {
  route_table_id         = aws_route_table.My-Route-Table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my-internet-gateway.id
}

resource "aws_route_table_association" "My-public-subnet-1-associate" {
  route_table_id = aws_route_table.My-Route-Table.id
  subnet_id      = aws_subnet.my-public-subnet-1.id
}

resource "aws_route_table_association" "My-public-subnet-2-associate" {
  route_table_id = aws_route_table.My-Route-Table.id
  subnet_id      = aws_subnet.my-public-subnet-2.id
}

resource "aws_route_table_association" "My-public-subnet-3-associate" {
  route_table_id = aws_route_table.My-Route-Table.id
  subnet_id      = aws_subnet.my-public-subnet-3.id
}