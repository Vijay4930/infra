terraform {
  required_version = "~>0.13.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.21"
    }
  }
  backend "s3" {
    bucket = "terrafrom-neww"
    key    = "dev/terrafrom.tfstate"
    region = "us-east-1"
  }
}
provider "aws" {
  region  = "us-east-1"
}