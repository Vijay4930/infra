resource "aws_instance" "My-Instance" {
    ami = "ami-0c02fb55956c7d316"
    instance_type = "t2.micro"
    key_name = "new-key-pair"
    user_data = <<-EOF
    #!/bin/bash
    sudo yum update -y
    sudo yum install httpd -y
    sudo systemctl enable httpd
    sudo systemctl start httpd
    echo "<h1>Welcome to StackSimplify ! AWS Infra created using Terraform in us-east-1 Region</h1>" > /var/www/html/index.html
    EOF
    tags = {
      "Name" = "My-Instance"
    }
}